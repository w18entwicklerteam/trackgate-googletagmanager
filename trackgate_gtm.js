trackGateNamespace.myGtm = function (oGate,sLocalConfData)
{
	window.MyGtmInnerWrapperSelf = this;
    this.oGate=oGate;
    this.sLocalConfData=sLocalConfData;
	
	
    this.index = function ()
    {

        this.initial();
		
		//Alle Funktionen an den GTM senden.
        this.oGate.registerFunctions("gtm","_all_",this.sendAll2Gtm);
    }

    this.initial = function ()
    {
		//Alle Container durchlaufen und setzen
		for(var i = 0; i < this.sLocalConfData.length; i++)
		{	
			
			this.getGtmContainer(this.sLocalConfData[i]);
		}	
    }
	
	this.getGtmContainer = function (sGtmId)
	{
		//Datalayer Variable für diesen Container setzen
		var sGtmName = sGtmId.replace(/\-/, "_");
		eval('window.dataLayer_' + sGtmName + ' = [];');
		
		//Container Basis Code setzen.
		(function (w, d, s, l, i) 
		{
		    w[l] = w[l] || [];
		    w[l].push({
		        'gtm.start': new Date().getTime(),
		        event: 'gtm.js'
		    });
		    var f = d.getElementsByTagName(s)[0],
		        j = d.createElement(s),
		        dl = l != 'dataLayer' ? '&l=' + l : '';
		    j.async = true;
		    j.src =
		        '//www.googletagmanager.com/gtm.js?id=' + i + dl;
		    f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer_'+sGtmName , sGtmId);
		
	}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++Tracker Functions+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    this.sendAll2Gtm = function (aFuncArg)
    {
		//Eine Trackingfunktion wurde aufgerufen -> an den GTM senden
		//var aFuncArg = this.sendAll2Gtm.arguments;
		var sFuncName = aFuncArg.shift();
		
		for(var i = 0 ; i < this.sLocalConfData.length; i++)
		{
			var sGtmId = this.sLocalConfData[i];
			var sGtmName = sGtmId.replace(/\-/, "_");
			
			//Alle Argumente (außer dem ersten, das ist der Funktionsname) als Datalayer Variable an den GTM senden
			for(var j = 0; j < aFuncArg.length; j++)
			{
				eval("window.dataLayer_" + sGtmName + ".push({'" + sFuncName + "_" + j + "':aFuncArg[" + j + "]});");	
			}
			
			//Das erste Argumente (das ist der Funktionsname) als Event an den GTM senden.
			eval("window.dataLayer_" + sGtmName + ".push({'event':'" + sFuncName + "'});");	
		}
    }

	
    this.index();
}

//Config für den GTM Wrapper registrieren (diese ist bereits im Einbaucode definiert)
trackGateNamespace.trackGateContainer.registerInnerConfig("gtm",trackGateNamespace.aGTM);
//Klasse beim Wrapper registrieren.
trackGateNamespace.trackGateContainer.registerInnerGate(trackGateNamespace.myGtm,"gtm");
